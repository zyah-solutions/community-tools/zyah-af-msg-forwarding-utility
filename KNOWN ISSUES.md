## AF Msg Forwarding Utility - Known Issues
### RT
- The deep folder location to which the Forwarding Utility gets deployed can cause problems in the Linux RT file system. In this case, it is advised to move the forwarding utility to another location on disk closer to your project source code.
- When deploying source code to Linux RT targets, the source code folders are flattened and seem use namespacing rather than the folder hierarchy (much like the LabVIEW 8.x file system). In order for the forwarding utility to work with this folder structure, any interfaces to forward must be owned by an .lvlib file of the same name.
    - e.g. "My Incoming Interface.lvclass" would need to be owned by "My Incoming Interface.lvlib".
- Forwarding (and localization) has not been tested on macOS.