# AF Msg Forwarding Utility Changelog

### v3.1.1 (2023/09/21)
- Added option to select Forwarding Map type on construction
- Fixed a bug that would sometimes select the incorrect Forwarding Map because of an error in version calculation (and added support for versioning in Linux RT as well).

### v3.1.0 (2023/09/21)
- Added self-registration tracking to avoid an issue where, in some cases, the unregistration of nested actors would also result in the calling actor being unregistered from those forwarding interfaces as well.
- Modified Debug API to accommodate new tracking (connector pane is different).
- NOTE: There are additional APIs that supercede some of the older APIs in v3.0.0. Any actor using the older APIs should replace them as follows:
	- Send Caller Add Nested Sink -> Register Self as Fwd Sink.vi
	- Send Caller Add Nested Source -> Register Self as Fwd Source.vi
	- Send Caller Remove Nested Sink -> Unregister Self as Fwd Sink.vi
	- Send Caller Remove Nested Source -> Unregister Self as Fwd Source.vi

### v3.0.0 (2023/05/23)
- Rewritten to forward according to registration instead of inheritance.
	- Upward forwarding actors can still also implement/override the interface methods themselves.
- Only the actors necessary to forward Msgs take the (very minimal) performance hit of doing so. Entire subtrees of the application can operate without forwarding enabled.
- Infinite feedback loops due to forwarding are eliminated.
- Forwarding Utility API includes more actor functionality for easier integration with other base actors.

### v2.1.0 (2023/04/06)
- Added the ability to append an actor's enqueuer to a map that already exists.
- Fixed a bug that limited Msg throughput when using the Msg --> Enqueuer map lookup (on by default). Msg throughput with this enabled should now be close to double what it was in previous versions.
- Renamed "AF Msg Path Map" to "Msg Destination Map for clarity"

### v2.0.1 (2022/06/23)
- Removed "Match Regular Expression" which uses XNode. (If built into a PPL, LabVIEW 2021 will crash when searching.)

### v2.0.0 (2022/05/23)
#### NOTE: Since some of the APIs have changed, this update may introduce breaking changes to your application. Please make sure you read the update notes and understand the implications before upgrading from a previous version.
- Changed forwarding actor default behavior to do application interface forwarding via composition vs an override of "Get Interfaces to Forward".
	- Use "Launch Forwarding Actor with Caller's Interfaces" to automatically composite the list of forwarding interfaces into any nested actor.
	- "Get Interfaces to Forward" was renamed to "Get Additional Interfaces to Forward". If desired, developers can use this method to specify forwarding interfaces by inheritance rather than by composition.
- Added a constructor method to specify the forwarding interfaces of any actor to be launched (e.g. a root actor) rather than relying on overrides of VIs.

### v1.3.2 (2022/03/16)
- Fixed a path issue with AF_Debug_Trace and AFDebug.
- Linux RT source deployment localization fix.

### v1.3.1 (2022/03/02)
- Fixed an issue with Linux RT path localization and source deployments (i.e. not RT EXEs)

### v1.3.0 (2022/02/24)
- Added Linux RT path localization
- Added automatic enqueuer removal for dynamically shutdown actors in the example base actor

### v1.2.1 (2022/01/12)
- Modified actor launching VIs to work with non-forwarding nested actors. i.e. Nested actors do not, themselves, need to be forwarding actors.
- Fixed a typo in documentation.

### v1.2.0 (2022/01/12)
- Added <Update Forwarding Map> VI to the AF Msg Forwarding Actor. This update wraps up other previously existing VIs in order to make porting the forwarding functionality to other (already existing) base actors easier.

### v1.1.0 (2021/10/27)
- Added Msg/interface lookup to improve Msg throughput performance.
- Included "Initialize Forwarding Utility" in an override of AF Msg Forwarding Actor's "Pre Launch Init" eliminating the need for child actors to create their own override.
- Added nested enqueuer output in "Launch Nested App Actor" to better align with "Launch Nested Actor".

### v1.0.0 (2021/10/17)
- Initial release