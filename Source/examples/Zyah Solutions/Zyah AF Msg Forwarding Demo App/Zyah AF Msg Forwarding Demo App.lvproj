﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">Example application that shows how to use the Zyah Actor Framework Message Forwarding utility.

Many icons derived from Icons8.</Property>
	<Property Name="ThirdParty.LV.ExampleFinder" Type="Str">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;nidna:ExampleProgram 
    xmlns:nidna="http://www.ni.com/Schemas/DNA/1.0" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:schemaLocation="http://www.ni.com/Schemas/DNA/1.0 ..\DNA\1.0\NiExampleProgram.xsd" 
    SchemaVersion="1.0" 
    ContentType="EXAMPLE" 
&lt;Title&gt;
	&lt;Text Locale="US"&gt;Zyah AF Msg Forwarding Demo App.lvproj&lt;/Text&gt;
&lt;/Title&gt;
&lt;Description&gt;
	&lt;Text Locale="US"&gt;Example application that shows how to use the Zyah Actor Framework Message Forwarding utility.&lt;/Text&gt;
&lt;/Description&gt;
&lt;Keywords&gt;
	&lt;Item&gt;actor&lt;/Item&gt;
	&lt;Item&gt;framework&lt;/Item&gt;
	&lt;Item&gt;message&lt;/Item&gt;
	&lt;Item&gt;messages&lt;/Item&gt;
	&lt;Item&gt;messaging&lt;/Item&gt;
	&lt;Item&gt;forwarding&lt;/Item&gt;
	&lt;Item&gt;utility&lt;/Item&gt;
	&lt;Item&gt;utilities&lt;/Item&gt;
&lt;/Keywords&gt;
&lt;Navigation&gt;
	&lt;Item&gt;3092&lt;/Item&gt;
&lt;/Navigation&gt;
&lt;FileType&gt;LV Project&lt;/FileType&gt;
&lt;Metadata&gt;
&lt;Item Name="RTSupport"&gt;&lt;/Item&gt;
&lt;/Metadata&gt;
&lt;ProgrammingLanguages&gt;
&lt;Item&gt;LabVIEW&lt;/Item&gt;
&lt;/ProgrammingLanguages&gt;
&lt;RequiredSoftware&gt;
&lt;NiSoftware MinVersion="20.0"&gt;LabVIEW&lt;/NiSoftware&gt; 
&lt;/RequiredSoftware&gt;</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Base Actors" Type="Folder">
			<Item Name="Demo App Base Actor.lvlib" Type="Library" URL="../Demo App Base Actor/Demo App Base Actor.lvlib"/>
			<Item Name="Forwarding Base Actor.lvlib" Type="Library" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Base Actor/Forwarding Base Actor.lvlib"/>
		</Item>
		<Item Name="Data Generators" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Fridge Actor.lvlib" Type="Library" URL="../Data Generators/Fridge Actor/Fridge Actor.lvlib"/>
			<Item Name="Fridge Announcements.lvlib" Type="Library" URL="../Data Generators/Fridge Announcements/Fridge Announcements.lvlib"/>
			<Item Name="Oven Actor.lvlib" Type="Library" URL="../Data Generators/Oven Actor/Oven Actor.lvlib"/>
			<Item Name="Oven Announcements.lvlib" Type="Library" URL="../Data Generators/Oven Announcements/Oven Announcements.lvlib"/>
		</Item>
		<Item Name="Displays" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Chart Display Actor.lvlib" Type="Library" URL="../Displays/Chart Display Actor/Chart Display Actor.lvlib"/>
			<Item Name="Digital Display Actor.lvlib" Type="Library" URL="../Displays/Digital Display Actor/Digital Display Actor.lvlib"/>
			<Item Name="Display Requests.lvlib" Type="Library" URL="../Displays/Display Requests/Display Requests.lvlib"/>
		</Item>
		<Item Name="Forwarding Utility" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Forwarding Map" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Forwarding Map - Abstract.lvclass" Type="LVClass" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Map - Abstract/Forwarding Map - Abstract.lvclass"/>
				<Item Name="Forwarding Map - By Object.lvclass" Type="LVClass" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Map - By Object/Forwarding Map - By Object.lvclass"/>
				<Item Name="Forwarding Map - By Path.lvclass" Type="LVClass" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Map - By Path/Forwarding Map - By Path.lvclass"/>
			</Item>
			<Item Name="Forwarding Utility Requests.lvlib" Type="Library" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Utility Requests/Forwarding Utility Requests.lvlib"/>
			<Item Name="Forwarding Wrapper Msg.lvclass" Type="LVClass" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Forwarding Wrapper Msg/Forwarding Wrapper Msg.lvclass"/>
			<Item Name="Zyah AF Msg Forwarding Utility.lvclass" Type="LVClass" URL="../../../../user.lib/Zyah Solutions/Zyah AF Msg Forwarding Utility/Zyah AF Msg Forwarding Utility/Zyah AF Msg Forwarding Utility.lvclass"/>
		</Item>
		<Item Name="Proxy Actor.lvlib" Type="Library" URL="../Proxy Actor/Proxy Actor.lvlib"/>
		<Item Name="Demo Launcher.vi" Type="VI" URL="../Demo Launcher.vi"/>
		<Item Name="Read Me.txt" Type="Document" URL="../Read Me.txt"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVMapReplaceAction.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMapReplaceAction.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Set Difference.vim" Type="VI" URL="/&lt;vilib&gt;/set operations/Set Difference.vim"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="AF Msg Forwarding Demo App EXE" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{04EE6556-3B5B-4862-8A4F-4EAC2E328791}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A3C237B6-1B75-4B6D-8F3D-91EF4E80768F}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{13656459-EAA1-462F-9549-C25B2BC814FA}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">AF Msg Forwarding Demo App EXE</Property>
				<Property Name="Bld_buildSpecName" Type="Str">AF Msg Forwarding Demo App EXE</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Builds/AF Msg Forwarding Demo App EXE</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8C76B21B-9A83-4219-8AEB-D0518E953A7A}</Property>
				<Property Name="Bld_version.build" Type="Int">20</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">AF Msg Forwarding Demo App.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Builds/AF Msg Forwarding Demo App EXE/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Builds/AF Msg Forwarding Demo App EXE/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{505F96CF-19CC-41DE-B876-1D5D5C1FAA2A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Demo Launcher.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Left Door Solutions</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">AF Msg Forwarding Demo App EXE</Property>
				<Property Name="TgtF_internalName" Type="Str">AF Msg Forwarding Demo App EXE</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2021 Left Door Solutions</Property>
				<Property Name="TgtF_productName" Type="Str">AF Msg Forwarding Demo App EXE</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{85348A52-857C-47B2-A8BE-937B01DAA698}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">AF Msg Forwarding Demo App.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
