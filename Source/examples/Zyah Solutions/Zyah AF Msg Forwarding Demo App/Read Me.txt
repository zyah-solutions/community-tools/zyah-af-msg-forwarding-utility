DEMO NOTES

Use "Demo Launcher.vi" to start the demo.

Notes:
- Use the GUI to launch data generator, data consumer or other proxy (i.e. middlemen) actors.
- Until a data generator has registered as a source of its messages, the messages are not sent to its caller. Likewise, if a data generator unregisters as a source of its messages, the messages cease to be sent to its caller.
- Overrides of BOTH announcement interfaces have already been implemented in both data consumer actors to save time in this demo, but in a real application, developers would have to create overrides in the actors.